# TrainControl

Keep the trains from hitting each other by clicking to stop and start them, but don't make the people at the stations wait too long. Made in Godot for a 10-hour game jam with the theme of control more than one.