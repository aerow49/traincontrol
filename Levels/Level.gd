extends Node2D

var isCrash = false
export var off:int

func _ready():
	for i in range($Stations.get_child_count()):
		for j in range($Stations.get_child(i).get_child_count()):
			$Stations.get_child(i).get_child(j).connect("fullStation",self,"_on_blue_crashed")

func _on_blue_crashed(pos):
	if not isCrash:
		isCrash = true
		for i in range(8):
			$Trains.get_child(i).isStopped = true
		for i in range(24):
			$Line2D.add_point(Vector2(pos.x+(off*cos(24*i)),pos.y+(off*sin(24*i))))
		$Timer2.start()
		$PlayerCamera.offset = pos
		$PlayerCamera.zoom = Vector2.ONE*4
		$Timer.stop()

func _on_Timer2_timeout():
	get_tree().change_scene("res://Levels/GameStart.tscn")