extends Node2D

var trainOn = 2
signal controlOn(num)


func _on_Timer_timeout():
	get_child(trainOn).visible = true
	emit_signal("controlOn",trainOn)
	trainOn+=1
	if trainOn < 8:
		get_node("../Timer").start()
