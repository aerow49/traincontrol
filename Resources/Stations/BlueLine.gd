extends HBoxContainer

export var train:String
var line
var cam

# Called when the node enters the scene tree for the first time.
func _ready():
	line = get_tree().get_root().get_child(0).get_node("Trains/"+train)
	cam = get_tree().get_root().get_child(0).get_child(0)
	$Button.text = "Stop"


func _on_Button_pressed():
	line.isStopped = not line.isStopped
	if line.isStopped:
		$Button.text = "Start"
	else:
		$Button.text = "Stop"


func _on_Button2_pressed():
	cam.offset = line.get_child(0).position
