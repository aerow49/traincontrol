extends Area2D

export var waitTime:float

signal fullStation(pos)

func _ready():
	$Timer.start(waitTime)

func _on_Station_area_entered(area):
	if area.get_parent().is_in_group("cart") and area.name == "Area2D" and get_parent().visible and area.get_parent().isHead:
		print(area.get_parent().get_parent().name)
		area.get_parent().get_parent().intoStation()
		$Timer.start(waitTime)


func _on_Timer_timeout():
	emit_signal("fullStation",position)
