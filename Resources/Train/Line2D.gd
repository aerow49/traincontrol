extends Line2D

var path:Path2D

func _ready():
	path = get_parent()
	for i in range(path.curve.get_point_count()):
		add_point(path.curve.get_point_position(i))
