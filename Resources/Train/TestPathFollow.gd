extends Path2D

export var speed:float
export var stationTime:float
export var color:Color
export (float, 0, 1) var trainOffset = 0
var isStopped:bool = false
onready var cart = preload("res://Resources/Train/TrainCart.tscn")
signal crashed(pos)

func _ready():
	randomize()
	for i in range(curve.get_point_count()):
		$Line2D.add_point(curve.get_point_position(i))
	$Line2D.add_point(curve.get_point_position(1))
	$Line2D.default_color = color
#	randomize()
#	var pos = randi()
	var base
	for i in range(randi()%7+3):
		var  newCart = cart.instance()
		newCart.connect("crashed",self,"didCrash")
		if(i==0):
			newCart.isHead = true
			base = rand_range(0,curve.get_baked_length())
		add_child(newCart)
		newCart.get_node("Area2D/Sprite").modulate = color
		newCart.offset-= base+115*i
	
func intoStation():
	$Timer.wait_time = stationTime
	$Timer.start()
	isStopped = true
	
func leaveStation():
	isStopped = false


func _on_Metro_visibility_changed():
	for i in range(get_child_count()):
		if get_child(i).is_in_group("cart"):
			get_child(i).get_node("Almost").set_monitorable(true)
			get_child(i).get_node("Area2D").set_monitorable(true)
			get_child(i).get_node("Almost").set_monitoring(true)
			get_child(i).get_node("Area2D").set_monitoring(true)
			
func didCrash(pos):
	emit_signal("crashed",pos)
