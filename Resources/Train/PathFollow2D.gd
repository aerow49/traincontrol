extends PathFollow2D
var trainName:String
var isTrain = true
var isHead = false

signal crashed(pos)

func _process(delta):
	if not get_parent().isStopped:
		unit_offset +=delta*get_parent().speed
		
func _on_Area2D_area_entered(area):
	if area.name == "Area2D" and area != get_node("../Area2D") and area.get_parent().get_parent() != get_parent():
#		print(get_parent().name+" & "+area.get_parent().get_parent().name)
#		get_tree().change_scene("res://Levels/GameStart.tscn")
		emit_signal("crashed",position)
