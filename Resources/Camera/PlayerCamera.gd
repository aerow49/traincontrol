extends Camera2D

export var speed:float
export var zoomSpeed:float

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_up"):
		offset.y-=speed*delta
	if Input.is_action_pressed("ui_down"):
		offset.y+=speed*delta
	if Input.is_action_pressed("ui_right"):
		offset.x+=speed*delta
	if Input.is_action_pressed("ui_left"):
		offset.x-=speed*delta
	if Input.is_action_pressed("zoom_out") and zoom.x <= 10:
		zoom += Vector2.ONE*delta*zoomSpeed
	if Input.is_action_pressed("zoom_in") and zoom.x >= 1:
		zoom -= Vector2.ONE*delta*zoomSpeed